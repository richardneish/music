Dirt simple music player
------

  * Clone this repository into a directory reachable by your web server
  * Run composer install to add dependencies
  * Copy config/config.php.dist to config/config.php and edit to suit
  * Go to play.php in a web browser
