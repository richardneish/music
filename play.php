<?php
require_once 'config/config.php';
require_once 'vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false // TODO: Enable caching through config.php
]);

$action = $_REQUEST['action'] ?? 'browse';
$path = $_SERVER['PATH_INFO'] ?? '/'; // FIXME: Sanitize the path

if ($action == 'play') {
    $song = MUSIC_BASE_DIR . $path;
    if (is_file($song) && is_readable($song)) {
        // play the requested song
        $length = filesize($song);

        header('Content-Type: audio/mpeg');
        header('Content-Disposition: filename=' . basename($song));
        header('Content-Length: ' . $length);

        $fp = fopen($song, "r");
        fpassthru($fp);
        fclose($fp);
    } else {
        echo "Error playing '$path'";
    }
} else if ($action == 'browse') {
    // list available songs
    $dirs = [];
    $files = [];
    foreach (new DirectoryIterator(MUSIC_BASE_DIR . $path) as $fileInfo) {
        if ($fileInfo->getFilename()[0] == '.') {
            // Skip hidden files and directories
            continue;
        }
        if ($fileInfo->isDir()) {
            array_push($dirs, $fileInfo->getFilename());
        } else {
            array_push($files, $fileInfo->getFilename());
        }
    }
    sort($dirs);
    sort($files);
    echo $twig->render('browse.html', [
        'base_path' => $_SERVER['SCRIPT_NAME'],
        'cwd' => $path,
        'dirs' => $dirs,
        'files' => $files
    ]);
} else if ($action == 'playall') {
    $files = [];
    header("Content-Type: audio/x-mpegurl");
    header('Content-Disposition: filename=' . basename($path) . '.m3u');
    foreach (new DirectoryIterator(MUSIC_BASE_DIR . $path) as $fileInfo) {
        if ($fileInfo->isFile() && preg_match('/.*mp3$/', $fileInfo->getFilename())) {
            array_push($files, $fileInfo->getFilename());
        }
    }
    echo $twig->render('playlist.m3u', [
        'base_url' => base_url(),
        'files' => $files
    ]);
} else if ($action == 'downloadall') {
    # enable output of HTTP headers
    $options = new \ZipStream\Option\Archive();
    $options->setSendHttpHeaders(true);

    # create a new zipstream object
    $zip = new \ZipStream\ZipStream('music.zip', $options);

    foreach (new DirectoryIterator(MUSIC_BASE_DIR . $path) as $fileInfo) {
        if ($fileInfo->isFile() && preg_match('/.*mp3$/', $fileInfo->getFilename())) {
            $zip->addFileFromPath($fileInfo->getFilename(), MUSIC_BASE_DIR . $path . '/' . $fileInfo->getFileName());
        }
    }
    # finish the zip stream
    $zip->finish();
}

function base_url(): string
{
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
    $host = $_SERVER['HTTP_HOST'];
    $uri = preg_replace('/^([^?]*)\?.*/', '$1', $_SERVER['REQUEST_URI']);

    return "$protocol://$host$uri";
}

?>
